package com.company.internetshop;

public class CustomerProfile {
    // для функціонування особистого кабінету замовника
    String name; // ПІП замовника
    String eMail; // електронна пошта замовника
    String phoneNumber; // контактний номер телефону замовника
    String address; // адреса доставки
    String shoppingHistory; // історія покупок

}
