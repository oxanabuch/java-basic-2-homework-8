package com.company.internetshop;

public class Contacts {
    // опис контактів інтернет-магазину
    String address; // адреса офісу інтернет-магазину
    String phoneNumber; // контактний телефон інтернет-магазину
    String representativeName; // ПІП представника інтернет-магазину
}
