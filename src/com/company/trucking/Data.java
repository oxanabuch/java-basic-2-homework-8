package com.company.trucking;

public class Data {
    // описує бажану та можливу хронологію перевезення
    String startData; // бажана дата завантаження вантажу
    String finishData; // бажана дата доставки вантажу
    String realStart; // фактична можлива дата завантаження вантажу
    String realFinish; // фактична можлива дата доставки вантажу

    // дату можна ввести за допомогою ініціалізації об'єкта Data та вивести за допомогою методу toString()
}
