package com.company.trucking;

public class Customer {
    // інформація про замовника
    String name; // назва компанії (ПІП замовника)
    String cargo; // назва вантажу
    String taxStatus; // статус платника податків
    String address; // адреса замовника

}
